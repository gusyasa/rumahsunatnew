<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Model;

class BlogentryModel extends Model
{
   protected $table = 'blog_entries';
}
