<?php

namespace App\Http\Controllers\Frontend\Blog;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Blog\BlogRepository;
use Illuminate\Support\Facades\DB;
use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
/**
 * Class HomeController.
 */
class BlogController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

      protected $blogentry;

    public function __construct(BlogRepository $blogentry)
    {
        //Instance repository UserRepository kedalam property $user
        $this->blogentry = $blogentry;
    }


    public function all()
    {
    	return $this->blogentry->showall();
    }   

    public function seoblog()
    {
        $title = 'Blog Artikel - Rumah Sunat Bali';
        $description = 'Rumah Sunat Bali - Layanan Sunat Anak, Remaja, & Dewasa';
        $url = url('/').'/blogs';
        $image = url('/frontend/assets/images/logo.png');
        $type = 'Article';

        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', $type);
        OpenGraph::addImages([$image]);

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

        JsonLd::setTitle($title);
        JsonLd::setDescription($description);
        JsonLd::addImage($image);
    }

    public function index()
    {
        $this->seoblog();

         $context = \JsonLd\Context::create('article', [
            'headline' => 'Rumah Sunat Bali Artikel tentang sirkumsisi',
            'description' => 'Semua hal yang wajib di ketahui sebelum sunat atau setelah sunat dilakukan',
            'mainEntityOfPage' => [
                'url' => 'https://rumahsunatbali.com',
            ],
            'datePublished' => '2020-11-06',
            'dateModified' => '2020-11-06',
            'author' => [
                'name' => 'dr. Aribudhi Nugraha',
            ],            
            'image' => [
                'url' =>  url('/frontend/images/blog/post/default.png'),
            ],
            'publisher' => [
                'name' => 'Rumah Sunat Bali',
                'logo' => [
                  'url' => url('frontend/assets/').'/images/logo.webp'
                ]
            ],
        ]);

        $alldata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(3);
        $blogsdata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(5);
        return view(getView('frontend.blogs'), ['blogsdata'=>$blogsdata],['alldata'=>$alldata, 'jsondata'=>$context] );
    }





    public function article($slug)
    {
        
    	$blogsdata = $this->blogentry->find($slug)->first(); 

        $title = $blogsdata->title;
        $description = $blogsdata->summary;
        $logo = url('frontend/assets/').'/images/logo.webp';
        $url = url('/').'/blog/'.$blogsdata->slug;
        $image = url('/').$blogsdata->image;
        $type = 'Article';
        $context = 'http://schema.org';
        $author = 'dr. Aribudhi Nugraha';
        $datePublished = $blogsdata->created_at->toDateString();
        $headline = $blogsdata->title;
        $publishername = 'Rumah Sunat Bali';
        $dateModified = $blogsdata->created_at->toDateString();
        $telephone = '6287777114800';
        $mainEntity = (object) array('@type' =>$type ,'@id'=>$url );
        $publisher = (object) array('@type' =>"Person" ,'name'=>$publishername );


        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', $type);
        OpenGraph::addImages([$image]);

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

        $context = \JsonLd\Context::create('article', [
            'headline' => $headline,
            'description' => $description,
            'mainEntityOfPage' => [
                'url' => 'https://rumahsunatbali.com/blogs',
            ],
            'image' => [
                'url' =>  $image,
                'height' => 800,
                'width' => 800,
            ],
            'datePublished' => $datePublished,
            'dateModified' => $dateModified,
            'author' => [
                'name' => $author,
            ],
            'publisher' => [
                'name' => 'Rumah Sunat Bali',
                'logo' => [
                  'url' => $logo
                ]
            ],
        ]);



        $alldata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(3);

        return view(getView('frontend.article'), ['blogsdata'=>$blogsdata, 'alldata'=>$alldata, 'jsondata'=>$context] );
        
    }
}
