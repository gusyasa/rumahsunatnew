<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Artesaos\SEOTools\Facades\SEOTools;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;


/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */


    public function index()
    {

    	$title = 'Rumah Sunat Bali';
        $name = 'Rumah Sunat Bali';
        $description = 'Layanan sunat(sirkumsisi) modern pertama di Bali, untuk usia bayi, anak hingga dewasa. Rumah sunat bali hadir di denpasar dan badung. Sunat ditangani dokter berpengalaman dengan metode sunat modern tanpa jahitan, (sunat klem, dan sunat stapler)';
        $telephone = '+6287777114800';
        $url = 'http://www.rumahsunatbali.com';
        $image = url('/frontend/assets/images/logo.png');

        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', 'Organization');
        OpenGraph::addImages([$image]);

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');

         $context = \JsonLd\Context::create('local_business', [
                'name' => $name,
                'description' => $description,
                'telephone' => $telephone,
                'openingHours' => 'sun,mon,tue,wed,thu,fri,sat',            
                'image' => [
                    url('/frontend/images/blog/post/default.png')
                ],
                 "priceRange"=>"$",
                'address' => [
                    'streetAddress' => 'Jl. Tukad Batanghari No.42, Panjer, Kec. Denpasar Bar., Kota Denpasar, Bali 80225',
                    'addressLocality' => 'Denpasar, Bali',
                    'addressRegion' => 'Denpasar',
                    'postalCode' => '80225',
                ],
                'geo' => [
                    'latitude' => '115.23022679341761',
                    'longitude' => '-8.680559463306608',
                ],
            ]);

    	$blogsdata = DB::table('blog_entries')->orderBy('id', 'desc')->paginate(6);
        return view(getView('frontend.index'), ['blogsdata'=>$blogsdata,'jsondata'=>$context] );
        //return view('frontend.index');
    }

    public function review()
    {

        $title = 'Testimony - Rumah Sunat Bali';
        $description = 'Rumah Sunat Bali - Layanan Sunat Anak, Remaja, & Dewasa';
        $url = 'http://www.rumahsunatbali.com/testimony';
        $image = url('/frontend/assets/images/logo.png');
        $telephone = '+6287777114800';
        
        SEOMeta::setTitle($title,false);
        SEOMeta::setDescription($description);
        SEOMeta::setCanonical($url);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($title);
        OpenGraph::setUrl($url);
        OpenGraph::addProperty('type', 'testimony');
        OpenGraph::addImages([$image]);

        TwitterCard::setTitle($title);
        TwitterCard::setSite('@rumahsunatbali');


         $context = \JsonLd\Context::create('local_business', [
                'name' => $title,
                '@id' => url("/"),
                'description' => $description,
                'telephone' => $telephone,
                'openingHours' => 'sun,mon,tue,wed,thu,fri,sat',
                'image' => [
                    url('/frontend/images/blog/post/default.png')
                ],   
                 "priceRange"=> "$",
                'address' => [
                    'streetAddress' => 'Jl. Tukad Batanghari No.42, Panjer, Kec. Denpasar Bar., Kota Denpasar, Bali 80225',
                    'addressLocality' => 'Denpasar, Bali',
                    'addressRegion' => 'Denpasar',
                    'postalCode' => '80225',
                ],
                'geo' => [
                    'latitude' => '115.23022679341761',
                    'longitude' => '-8.680559463306608',
                ],
            ]);

        
        return view(getView('frontend.testimony'),['jsondata'=>$context]);
    }

    
}
