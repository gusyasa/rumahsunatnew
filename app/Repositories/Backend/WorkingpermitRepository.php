<?php

namespace App\Repositories\Backend;


use App\Exceptions\GeneralException;
use App\Models\WorkingpermitModel;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRepository.
 */
class WorkingpermitRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param  User  $model
     */
    protected $workingpermit;

    public function __construct(WorkingpermitModel $workingpermit)
    {
        $this->workingpermit = $workingpermit;
    }

    //MEMBUAT FUNGSI UNTUK MENGAMBIL DATA YANG TELAH DI PAGINATE
    //DAN DIFUNGSI INI TELAH DIURUTKAN BERDASARKAN CREATED_AT
    //FUNGSI INI MEMINTA PARAMETER JUMLAH DATA YANG AKAN DITAMPILKAN
    public function getPaginate($per_page)
    {
        return $this->workingpermit->orderBy('created_at', 'DESC')->paginate($per_page);
    }   


    public function showall()
    {
        return $this->workingpermit::all();
    }

    //MEMBUAT FUNGSI UNTUK MENGAMBIL DATA BERDASARKAN ID
    public function find($id)
    {
        return $this->workingpermit->find($id);
    }

    //MEMBUAT FUNGSI UNTUK MENGAMBIL DATA BERDASRAKAN COLOMN YANG DITERIMA
    public function findBy($column, $data)
    {
        return $this->workingpermit->where($column, $data)->get();
    }
}
