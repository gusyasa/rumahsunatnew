<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */
    'frontend'=>[
        'header' => [
            'home' => 'Home',
            'metode' => 'Metode',
            'service' => 'Layanan',            
            'testimony' => 'Testimony',
            'blog' => 'Blog',
            'paket' => 'Paket Harga',
            'kontak' => 'Kontak',
        ],
        'footer' => [
            'text' => 'Layanan Kami',
            'list' => [
                'sunatbalita' => 'Sunat Balita',
                'sunatanak' => 'Sunat Anak',
                'sunatremaja' => 'Sunat Remaja',
                'sunatdewasa' => 'Sunat Dewasa',
                'sunatgemuk' => 'Sunat Gemuk',
                'sunatkhusus' => 'Sunat Kondisi Khusus',
                'homecare' => 'Home Care Service',
            ]
        ]
    ],
   
];
