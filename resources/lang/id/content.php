<?php

return [
    'slider' => [
        'one' => 'image1-id.webp',
        'two' => 'image2-id.webp',
        'three' => 'image3-id.webp',
        'four' => 'image4-id.webp',
    ],
    'section' => [   
        'welcome' => [
            'head' => 'Selamat datang di Rumah Sunat Bali',
            'title' => 'Welcome to Rumah Sunat Bali',
            'text' => 'Layanan kesehatan rawat jalan modern berbasis  dokter praktek khusus untuk sunat / sirkumsisi . pelayanan dengan standarisasi yang tinggi dan dikerjakan oleh dokter yang berpengalaman dalam tindakan sirkumsisi dari usia bayi hinga dewasa.',
            'metode'=>[
                'judul'=>[
                    'head'=>'Metode Sunat Kami',
                    'text'=>'Berikut adalah metode sunat yang kami gunakan'
                ],   
                'conventional'=>[
                    'head' =>'SUNAT KONVENSIONAL / CAUTERY',
                    'text' =>'Sunat dengan metode bedah minor, dengan luka jahitan konvensional. ',
                ],
                'clamp'=>[
                    'head' =>'SUNAT CLAMP / KLEM',
                    'text' =>'Sunat dengan metode clamp , menggunakan device khusus yang mencetak hasil sunat sehingga hasil rapi sesuai device clamp yang digunakan. Metode ini tanpa menggunakan jahitan kulit pada luka. ',
                ],
                'bipolar'=>[
                    'head' =>'SUNAT METODE BIPOLAR CAUTERY ',
                    'text' =>'Sunat dengan device bipolar cautery , penyempurnaan metode konvensional dengan hasil yang lebih aestetis dan minim resiko perdarahan. ',
                ],
                'stapler'=>[
                    'head' =>'SUNAT STAPLER',
                    'text' =>'Sunat dengan metode stapler, mengadopsi teknik konvensional dank lamp  dengan bantuan device stapler untuk penyatuan kulit, praktis, nyaman dan hasil rapih.',
                ],

            ]
        ],       

        'metode' => [
            'head' => 'METODE SUNAT KAMI',
            'title' => 'Our Circumcision Methods',
            'text' => 'Rumah Sunat Bali menyediakan berbagai pilihan metode dalam sirkumsisi sesuai dengan kebutuhan dan pemeriksaan oleh dokter kami, untuk sunat mulai usia bayi hingga dewasa”.',
        ], 

        'testimony' => [
            'head' => 'Testimoni Client Kami',
            'text' => 'Silakan lihat testimony client kami',
            'button' => 'Lihat Testimony',
        ],

        'cta' => [
            'head' => 'Yuk Tanya!',
            'text' => 'Sangat penting menemukan tempat Sunat yang aman dan terpercaya',
        ],

        'ctacall' => [
            'head' => '<span>Yuk Booking, </span>Bagaimana cara Booking <br> Rumah Sunat Bali?',
            'button' => 'Kontak Sekarang'
        ],
        'footer'=>[
            'text'=>'Rumah Sunat Bali memberikan pengalaman professional dalam menjalankan proses Sirkumsisi/Sunat dengan layanan dan metode yang sesuai dengan prosedur tindakan medis'
        ],

        'layanan' => [
            'cta'=>'Yuk! cari tahu tentang Rumah Sunat Bali',
            'layanan'=>'Layanan Kami',
            'kontak'=>'Kontak Kami',
            'option' => [
                'sunatbayianak'=>[
                    'head' => 'Sunat Bayi & Anak',
                    'text' => 'Sunat pada usia 0 sampai 12 tahun menggunakan Metode Konvensional bedah dengan Guillotine teknik atau metode sunat laser',
                ],
                'sunatremaja'=>[
                    'head' => 'Sunat Remaja',
                    'text' => 'Sunat pada usia 12 sampai 17 tahun menggunakan Metode Konvensional Bedah dan Sunat Stapler',
                ],
                'sunatdewasa'=>[
                    'head' => 'Sunat Dewasa',
                    'text' => 'Sunat Dewasa untuk laki laki diatas umur 17 tahun dengan pilihan Metode Sunat Konvensional (Sunat Laser) dan Sunat Stapler',
                ],
                'sunatgemuk'=>[
                    'head' => 'Sunat Gemuk',
                    'text' => 'Sunat Gemuk untuk pasien memiliki berat diatas normal usia dan Indeks Masa Tubuh, dengan metode Konvensional dan Clamp (Klem)',
                ],
                'sunatkhusus'=>[
                    'head' => 'Sunat Kondisi Khusus',
                    'text' => 'Sunat Kondisi Khusus seperti adanya perbaikan hasil sunat yang tidak rapi, kulup yang masih panjang, pembengkakan kulup atau abnormal',
                ],
                'sunatkhusus'=>[
                    'head' => 'Sunat Kondisi Khusus',
                    'text' => 'Sunat Kondisi Khusus seperti adanya perbaikan hasil sunat yang tidak rapi, kulup yang masih panjang, pembengkakan kulup atau abnormal',
                ],
                'homecare'=>[
                    'head' => 'Home Care Service',
                    'text' => 'Tindakan sunat / rawat luka dirumah dilaksanakan untuk kondisi khusus yang telah dikonfirmasi dokter di rumah sunat bali.',
                ],
            ]
        ],
    ],
    'paket' => [
        'text'=>'Paket Harga',
        'welcome' =>[
            'head'=>'Sirkumsisi/Sunat',
            'text'=>'Semua paket harga tindakan sunat sudah termasuk konsultasi dokter, tidakan sunat, obat dan rawat luka dalam rentang penyembuhan normal.'
        ],
        'qna'=>[
            'one'=>[
                'question'=>'USIA BAYI & ANAK MAKSIMAL UMUR TAHUN',
                'answer'=>' <p>Usia bayi & anak maksimal umur 12 th : mulai 950.000  - 2.500.000</p>'
            ], 
            'two'=>[
                'question'=>'USIA REMAJA 13 - 17 TAHUN',
                'answer'=>'<p>Usia remaja 13 th -17 th : mulai 1.400.000 sampai 2.750.000</p>'
            ],    
            'three'=>[
                'question'=>'USIA DEWASA',
                'answer'=>'<p>Usia dewasa : mulai 2.000.000 sampai 3.500.000</p>'
            ],    
        ],
        'closingparagraph'=>'Detail biaya akan dijelaskan saat konsultasi sesuai dengan method yang digunakan . Perubahan harga dapat terjadi sewaktu – waktu dan dijelaskan oleh admin kami saat konsultasi.'
    ],
    
];
