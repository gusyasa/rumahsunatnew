@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="content-wrapper fade-in" >   
    <section class="content" v-if="openTab">
        <div class="container-fluid">
            <!-- Main row -->
            <div class="row">
                <input type="hidden" id="idUser" value="{{$idUser ?? ''}}">
                <!-- Left col -->
                <transition name="slide-fade">
                    <div class="col-md-12" v-if="showTableWorkingPermit">
                        <!-- TABLE: LATEST ORDERS -->
                         <table-workingpermit ref="tableWorkingPermit" @data-workingpermit="getDataWorkingPermit">
                                </table-workingpermit>

                    </div>
                </transition>                

                <transition name="slide-fade">
                    <div class="col-md-12" v-if="showFormWorkingPermit">
                        <!-- TABLE: LATEST ORDERS -->
                         <table-workingpermit ref="tableWorkingPermit" @data-workingpermit="getDataWorkingPermit">
                                </table-workingpermit>

                    </div>
                </transition>
            </div>
        </div>
    </section>

</div>


@endsection

@section('pagespecificscripts')
    {!! script(mix('js/dashboard.js')) !!}
@stop