@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

<!--Page Title-->
    <section class="page-title" style="background-image:url({{ url('frontend/assets/')}}/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h3>@lang('content.paket.text')</h3>
                <ul class="bread-crumb">
                    <li><a href="{{url('/paketharga')}}">Home</a></li>
                    <li>@lang('content.paket.text')</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->

    <section id="metode">
        <section class="featured-section style-two">
            <div class="auto-container">
                <!--Sec Title-->
                <div class="sec-title centered">
                    <div class="title-icon"><img src="{{ url('frontend/assets/')}}/images/icons/sec-title-icon-1.webp" alt=""></div>
                    <h1>@lang('content.section.welcome.metode.judul.head')</h>
                    <div class="text">@lang('content.section.welcome.metode.judul.text')</div>
                    
                </div>
                <!--End Sec Title-->
                <div class="row clearfix">
                    <!--Column/ Pull Left-->
                    <div class="column pull-left col-md-4 col-sm-6 col-xs-12">
                        <!--Feature Block-->
                        <div class="feature-block">
                            <div class="inner-box">
                                <div class="icon-box">
                                    <img src="{{ url('frontend/assets/')}}/images/metode/konvensional.webp" alt="">
                                </div>
                                <h3><a>@lang('content.section.welcome.metode.conventional.head')</a></h3>
                                <div class="text">@lang('content.section.welcome.metode.conventional.text')</div>
                            </div>
                        </div>
                        
                        <!--Feature Block-->
                        <div class="feature-block">
                            <div class="inner-box">
                                <div class="icon-box">
                                    <img src="{{ url('frontend/assets/')}}/images/metode/klam.webp" alt="">
                                </div>
                                <h3><a>@lang('content.section.welcome.metode.clamp.head')</a></h3>
                                <div class="text">@lang('content.section.welcome.metode.clamp.text')</div>
                            </div>
                        </div>
                        
                    </div>
                    <!--Column / Pull Right-->
                    <div class="column pull-right col-md-4 col-sm-6 col-xs-12">
                        <!--Feature Block Two-->
                        <div class="feature-block-two">
                            <div class="inner-box">
                                <div class="icon-box">
                                    <img src="{{ url('frontend/assets/')}}/images/metode/ring.webp" alt="">
                                </div>
                                <h3><a>@lang('content.section.welcome.metode.bipolar.head')</a></h3>
                                <div class="text">@lang('content.section.welcome.metode.bipolar.text')</div>
                            </div>
                        </div>
                        
                        
                        <!--Feature Block Two-->
                        <div class="feature-block-two">
                            <div class="inner-box">
                                <div class="icon-box">
                                    <img src="{{ url('frontend/assets/')}}/images/metode/stapler.webp" alt="">
                                </div>
                                <h3><a>@lang('content.section.welcome.metode.stapler.head')</a></h3>
                                <div class="text">@lang('content.section.welcome.metode.stapler.text')</div>
                            </div>
                        </div>
                        
                    </div>
                
                    <div class="image-column col-md-4 col-sm-12 col-xs-12">
                        <figure class="image wow fadeInUp">
                            <img src="{{ url('frontend/assets/')}}/images/resource/metodekami1.webp" alt="">
                        </figure>
                    </div>
                    
                </div>
                
            </div>
        </section>
    </section>

@endsection