@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')


    <!--Page Title-->
    <section class="page-title" style="background-image:url({{ url('frontend/assets/')}}/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h5>blog Details</h5>
                <ul class="bread-crumb">
                    <li><a href="index.html">Home</a></li>
                    <li>Blog</li>
                    <li>blog Details</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Sidebar Page-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!--Blog-->
                    <section class="blog-single">
                        
                        <!--News Style Four-->
                        <div class="news-style-four">
                            <div class="inner-box">
                                <!--Image Column-->
                                <div class="image">
                                    <img src="{{$blogsdata->image}}" alt="">
                                </div>
                                <!--Content Column-->
                                <div class="content-column">
                                    <div class="inner">
                                        <div class="post-date">{{$blogsdata->updated_at}}</div>
                                        <h1>{{$blogsdata->title}}</h1>
                                        <ul class="post-meta">
                                            <li>by <span>dr. Aribudhi Nugraha</span></li>
                                        </ul>
                                        <div class="text">
                                            <p>{!!$blogsdata->content!!}</p>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                 
                        
                    </section>
                    
                </div>
                
                <!--Sidebar-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar">
                        
                        
                        <!--Services Post Widget-->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title">
                                <h3>Latest Post</h3>
                            </div>
                            <!--Post-->
                            @foreach($alldata as $latestpost)
                            <article class="post">
                                <figure class="post-thumb img-circle"><a href="{{url('blog/'.$latestpost->slug)}}"><img width="70px" height="70px" src="{{$latestpost->image}}" alt=""></a></figure>
                                <div class="text"><a href="{{url('blog/'.$latestpost->slug)}}">{{$latestpost->title}}</a></div>
                                <div class="post-info">Posted by {{$latestpost->author_name}}</div>
                            </article>
                            @endforeach
                            <!--Post-->
                        </div>
                        
                        
                    </aside>
                </div>
                
            </div>
        </div>
    </div>


@endsection