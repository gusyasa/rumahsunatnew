
 <nav class="navbar navbar-default navbar-static-top navbar2">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html"><img src="{{ url('frontend/')}}/images/logo/3.png" alt=""></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav" aria-expanded="false">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="book-appointment.html" class="navbar-toggle visible-xs" data-toggle="modal" data-target="#appointmefnt_form_pop">book appointment</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->            
            <div class="collapse navbar-collapse" id="main_nav">                
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Home</a>
                    </li>
                    <li><a href="time-table.html">Layanan</a></li>
                    <li><a href="time-table.html">Metode</a></li>
                    <li><a href="time-table.html">Harga</a></li>
                    <li><a href="time-table.html">Pertanyaan Umum</a></li>
                    <li><a href="time-table.html">News</a></li>
                    <li><a href="contact.html">Kontak</a></li>
                    <li class="hidden-xs book"><a href="#" data-toggle="modal" data-target="#appointmefnt_form_pop">book appointment</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
            


        </div><!-- /.container -->
    </nav>
    