@extends('frontend.layouts.app',['jsondata'=>$jsondata])

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <section class="page-title" style="background-image:url(frontend/assets/images/background/5.jpg);">
        <div class="auto-container">
            <div class="inner-box">
                <h1>Testimony</h1>
                <ul class="bread-crumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li>Review & Testimony</li>
                </ul>
            </div>
        </div>
    </section>

    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                @foreach (range(1,19) as $testi)
                    <div class="gallery-item col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image-box"><img src="{{ asset('/testimony/').'/'.$testi.'.jpeg'}}" alt="">
                                <div class="overlay-box">
                                    <div class="content">
                                        <a class="lightbox-image" href="{{ asset('/testimony/').'/'.$testi.'.jpeg'}}" title="Testimony User" data-fancybox-group="example-gallery"><span class="icon flaticon-plus"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                

            </div>
            
            <!-- Styled Pagination -->
          
        </div>
    </div>   

@endsection
