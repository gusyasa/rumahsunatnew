@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

    <!--Page Title-->
    <section class="page-title" style="background-image:url(frontend/assets/images/background/5.jpg);">
    	<div class="auto-container">
        	<div class="inner-box">
                <h1>404 Error</h1>
                <ul class="bread-crumb">
                	<li><a href="index.html">Home</a></li>
                    <li>Pages</li>
                    <li>404 Error</li>
                </ul>
            </div>
        </div>
    </section>
    <!--End Page Title-->
    
    <!--Error Section-->
    <section class="error-section">
    	<div class="auto-container">

            <h2>Oops! Page Not Found</h2>
            <div class="text">Page you were looking for could not be found. </div>
            <a class="theme-btn btn-style-one" href="{{url('/')}}"> Back to Home</a>
        </div>
    </section>
    <!--End Error Section-->

    

@endsection
